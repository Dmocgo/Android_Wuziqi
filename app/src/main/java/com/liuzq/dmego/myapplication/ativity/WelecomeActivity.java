package com.liuzq.dmego.myapplication.ativity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.liuzq.dmego.myapplication.R;

public class WelecomeActivity extends Activity {

    private Button bt1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welecome);

        bt1 = (Button)findViewById(R.id.game_start);

        //注册点击事件
        bt1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                /**
                 * 第一个参数：上下文对象this
                 * 第二个参数:目标文件
                 */
                Intent intent = new Intent(WelecomeActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
